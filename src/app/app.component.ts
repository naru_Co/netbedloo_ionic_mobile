import { Component, ViewChild } from '@angular/core';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';
import {AuthProvider} from "../providers/auth/auth";
import {TutorialPage} from "../pages/tutorial/tutorial"
import { Settings } from '../providers/providers';
import {Principal} from "../providers/providers"
import {Storage} from "@ionic/storage";
import {OneSignal} from '@ionic-native/onesignal';
import { Network } from '@ionic-native/network';

@Component({
  template: `
  <ion-nav #content [root]="rootPage"></ion-nav>`
  /*<ion-menu [content]="content">
    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          {{p.title}}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>*/
})
export class MyApp {
  rootPage : any = null;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Tabs', component: 'TabsPage' },
    { title: 'Login', component: 'LoginPage' },
    { title: 'Signup', component: 'SignupPage' },
    { title: 'Settings', component: 'SettingsPage' },
  ]

  constructor(private _OneSignal: OneSignal,private translate: TranslateService, private platform: Platform, settings: Settings, private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen,public authProvider: AuthProvider, private principal : Principal, private storage : Storage,private network: Network) {
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.nav.push('ConnectionProbPage')
    });
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      this.nav.pop();});
    this.initTranslate();
     platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      
      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };
  
      this._OneSignal
        .startInit("ea9761a2-26c1-4047-b09d-40ff05f1b6da", "979930902317")
        .inFocusDisplaying(_OneSignal.OSInFocusDisplayOption.None)
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
      
      
    });

    authProvider.authUser.subscribe(jwt => {
      
      
      if (jwt) {
        this.rootPage = 'TabsPage';
      }
      else {
        this.rootPage = 'TutorialPage';
      }
    });

    authProvider.checkLogin();
    this.principal.identity().then((account) => {
      this.storage.set('userInformation', JSON.stringify(account))
      
  });
  }

  

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


  
}
