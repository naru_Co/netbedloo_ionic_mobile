import { ErrorHandler, NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { AutoCompleteModule } from 'ionic2-auto-complete';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Crop } from '@ionic-native/crop';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {AuthHttp, AuthConfig,JwtHelper} from 'angular2-jwt';
import { Items } from '../mocks/providers/items';
import { Settings } from '../providers/providers';
import { User } from '../providers/providers';
import { Api } from '../providers/providers';
import { ParseLink } from '../providers/providers';
import {LanguageService} from '../providers/providers'
import { MyApp } from './app.component';
import { CameraServiceProvider } from '../providers/camera-service/camera-service';
import { CountriesProvider } from '../providers/countries/countries';
import { AuthProvider } from '../providers/auth/auth';
import { LocalsProvider } from '../providers/locals/locals';
import { Principal } from '../providers/principal/principal';
import {AccountService} from '../providers/auth/account';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { WindowRefProvider } from '../providers/window-ref/window-ref';
import { PrivateChatProvider } from '../providers/private-chat/private-chat';
import { ChatNotifProvider } from '../providers/private-chat/chat-notif';
import { Pro } from '@ionic/pro';
import { MomentModule } from 'angular2-moment';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';

import { SuperTabsModule } from 'ionic2-super-tabs';
import { MatchiNotifProvider } from '../providers/matchi-notif/matchi-notif';
import { GlobalVarsProvider } from '../providers/global-vars/global-vars';
import {OneSignal} from '@ionic-native/onesignal';
import { Network } from '@ionic-native/network';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Geolocation } from '@ionic-native/geolocation';
import { SocialSharing } from '@ionic-native/social-sharing';


/* const IonicPro = Pro.init('76a50e59', {
  appVersion: "1.1.1"
}); */

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.


export function authHttpServiceFactory(http: Http, storage: Storage) {
  return new AuthHttp(new AuthConfig({
    headerPrefix: 'Bearer',
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],        
    tokenGetter: (() => storage.get('authenticationToken').then((token: string) => token)),
  }), http);
}
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
export class MyErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    //IonicPro.monitoring.handleNewError(err);
  }
}
export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    MomentModule,    
    BrowserModule,
    AutoCompleteModule,
    FormsModule,
    HttpModule,
    
    SuperTabsModule.forRoot(),    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp,{
      scrollAssist: true,
      autoFocusAssist: true,
      tabsHideOnSubPages:false,  
      tabSubPages: false
      
    }),
    IonicImageViewerModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

  ],
  providers: [
    Api,
    OneSignal,
    ParseLink,
    Items,
    User,
    Camera,
    JwtHelper, 
    GoogleMaps,
    SplashScreen,
    Crop,
    Network,
    NativeGeocoder,
    BackgroundMode,
    Geolocation,
    LocalNotifications,
    StatusBar,
    SocialSharing,
    { provide: ErrorHandler, useClass: MyErrorHandler },
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CameraServiceProvider,
    CountriesProvider,
    AuthProvider,
    LanguageService,
    JwtHelper, {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, Storage]
    },
    LocalsProvider,
    Principal,
    AccountService,
    WindowRefProvider,
    PrivateChatProvider,
    ChatNotifProvider,
    MatchiNotifProvider,
    GlobalVarsProvider,
    
  ]
})
export class AppModule { }
