import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFeedModalPage } from './add-feed-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddFeedModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddFeedModalPage),
    TranslateModule.forChild() 
  ],
})
export class AddCommentModalPageModule {}
