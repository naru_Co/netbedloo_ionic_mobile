import { Component, ViewChild } from '@angular/core';
import { Refresher, Events, IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { Api } from '../../providers/providers';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the AddItemModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-feed-modal',
  templateUrl: 'add-feed-modal.html',
})
export class AddFeedModalPage {
  @ViewChild('fileInput') fileInput;
  currentItems: any;
  feed: any;
  comment: any
  profile: any;
  valeur: any;
  findBook: boolean;
  findGame: boolean;
  toChip: boolean;
  itemQuote: any;
  loading: any;
  hideSave:boolean;
  saving:string;
  feedPosted:string;
  wrong:string;

  constructor(public translateService: TranslateService,public events: Events, private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private api: Api, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    this.profile = navParams.get('profile')
    this.findBook = false;
    this.findGame = false;
    this.toChip = false;
    this.hideSave=false
    this.itemQuote = {id:null}
    this.feed = this.formBuilder.group({
      content: ['', Validators.compose([Validators.required, Validators.maxLength(1500)])]
    });
    translateService.get('SAVING').subscribe(
      value => {
        // value is our translated string
        this.saving = value;
      })
    translateService.get('NEW_POST_SUCC').subscribe(
      value => {
        // value is our translated string
        this.feedPosted = value;
      })
      translateService.get('WRONG').subscribe(
        value => {
          // value is our translated string
          this.wrong = value;
        })
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content:  this.saving,
    });
  }

  ionViewDidLoad() {

  }

  async addFeed(form: NgForm) {
    try {
      if(this.itemQuote.id!=null){
      const query = await this.api.post('feeds', {
        owner: this.profile,
        article: this.itemQuote,
        content: this.feed.value.content
      }).subscribe((res) => {
        this.loading.dismiss();
        this.events.publish('feed:added', res.json());
        this.backToBooks()
      })
    }else{
      const query = await this.api.post('feeds', {
        owner: this.profile,
        content: this.feed.value.content
      }).subscribe((res) => {
        this.loading.dismiss();
        this.events.publish('feed:added', res.json());
        this.backToBooks()
      })

    }
      this.creationToast(this.feedPosted);
      this.feed.reset();
    } catch (err) {
      this.creationToast(this.wrong);
      this.feed.reset();


    }
  }

  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  async makeAchip(item) {
    const query = await this.api.get('articles/'+item.id).subscribe((res) => {
      this.itemQuote = res.json();
    })
    this.hideSave=false
    this.toChip = true;
  
  }
  deleteChip(chip: Element) {
    chip.remove();
    this.itemQuote.reset()
    this.currentItems = [];
    this.hideSave=false
    this.findBook = false;
    this.findGame = false;
    this.toChip = false;
  }
  abortMission(){
    this.itemQuote.reset()
    this.currentItems = [];
    this.hideSave=false
    this.findBook = false;
    this.findGame = false;
    this.toChip = false;
  }
  findMyBookToQuote() {
    this.findBook = true;
    this.hideSave=true
  }
  findMyGameToQuote() {
    this.findGame = true;
    this.hideSave=true
  }
  getBooks(ev) {
    let val = ev.target.value;
    this.valeur = val;
    console.log(this.valeur)
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.api.get("booksbytitleandcategory/1101/" + val).subscribe((res) => this.currentItems = res.json())


  }
  getGames(ev) {
    let val = ev.target.value;
    this.valeur = val;
    console.log(this.valeur)
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.api.get("booksbytitleandcategory/1102/" + val).subscribe((res) => this.currentItems = res.json())


  }


  doRefresh(refresher: Refresher) {
    this.dismiss()
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  backToBooks() {
    this.navCtrl.push('FeedPage', {
      animate: true,
      direction: 'forward'
    })
    /* this.navCtrl.setRoot('FeedModalPage', { feed: this.feed,profile:this.profile }, {
       animate: true,
       direction: 'forward'
     });*/
  }


}
