import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddItemModalPage } from './add-item-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddItemModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddItemModalPage),
    TranslateModule.forChild() 
  ],
})
export class AddItemModalPageModule {}
