import { Component,ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,LoadingController,ToastController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import {Api} from '../../providers/providers';

/**
 * Generated class for the AddItemModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-item-modal',
  templateUrl: 'add-item-modal.html',
})
export class AddItemModalPage {
  @ViewChild('fileInput') fileInput;
  
  article: any;
  edit=false;
  imagePath:any;
  ind=false;
  loading:any;
  profile:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public camera: Camera,private api:Api,public loadingCtrl: LoadingController,public toastCtrl: ToastController ) {
    this.article = navParams.get('book')
    this.profile = navParams.get('profile')
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Your item is being Saved',
    });
  }

  ionViewDidLoad() {

  }

  save(){
    if(this.article.article.description.length<1){
      this.article.article.description="Description not found."
    }
    this.loading.present();
  this.api.post('articles',this.article).subscribe((res)=>{this.loading.dismiss();
    this.backToBooks(res.json())},(err)=>{this.loading.dismiss();})

 }


  dismiss() {
    this.viewCtrl.dismiss();
  }
  editArticle(){
    this.edit=true;
  }
  backToBooks(article) {
    this.navCtrl.setRoot('BookModalPage', { Newbook: article,profile:this.profile }, {
      animate: true,
      direction: 'forward'
    });
  }
  getPictureFromPhone(){
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 600,
        targetHeight: 1000,
        saveToPhotoAlbum:true
      }).then((fileUri) => {
        this.imagePath =  'data:image/PNG;base64,'+fileUri;
        this.article.file=fileUri;
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }

  }

  getPicture() {
    
      if (Camera['installed']()) {
        this.camera.getPicture({
          destinationType: this.camera.DestinationType.DATA_URL,
          targetWidth: 600,
          targetHeight: 1000,
          saveToPhotoAlbum:true
        }).then((fileUri) => {
          this.imagePath =  'data:image/PNG;base64,'+fileUri;
          this.article.file=fileUri;
          
        }, (err) => {
          alert('Unable to take photo');
        })
      } else {
        this.fileInput.nativeElement.click();
      }
    }

}
