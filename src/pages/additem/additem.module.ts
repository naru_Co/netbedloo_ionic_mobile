import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdditemPage } from './additem';
import { TranslateModule } from '@ngx-translate/core';
import { AutoCompleteModule } from 'ionic2-auto-complete';

@NgModule({
  declarations: [
    AdditemPage,
  ],
  imports: [
    AutoCompleteModule,
    IonicPageModule.forChild(AdditemPage),
    TranslateModule.forChild()
    
  ],
})
export class AdditemPageModule {}
