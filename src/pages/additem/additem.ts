import { Component, ViewChild } from '@angular/core';
import {ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController,AlertController } from 'ionic-angular';
import {Api} from '../../providers/providers';
import {Http } from "@angular/http";
import { TranslateService } from '@ngx-translate/core';
import {Principal} from "../../providers/providers";
import {Crop} from '@ionic-native/crop'

/**
 * Generated class for the AdditemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  
  
  selector: 'page-additem',
  templateUrl: 'additem.html',
})
export class AdditemPage {
  @ViewChild('fileInput') fileInput;
  currentItems: any = [];
  
  isReadyToSave: boolean;
  form: FormGroup;
  type:Number=0;
  languages:any;
  searchCountryString :any;
  selectedlanguage :any;
  bookName:any;
  selectedBook:any;
  imagePath:any;
  newArticle:any={
    article:{spec:{}},
    type:null,
    file:null
  }
  imgHeight:any;
  imgWidth:any;
  currentCategory:any;
  currentProfile:any;
  val:any;
  availableBooks:any;
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, formBuilder: FormBuilder, public camera: Camera,public crop : Crop,private api:Api,private readonly http: Http,public alertCtrl: AlertController, translate: TranslateService, private principal : Principal,public modalCtrl: ModalController) {


    this.form = formBuilder.group({
      profilePic: ['',Validators.required],
      title: ['', Validators.required],
      author: ['',Validators.required],
      language:['',Validators.required],
      description:['',Validators.required]
    });

    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  openModal2(){
    let bookModal = this.modalCtrl.create('CheckPleasePage');
    bookModal.present();
  }
 

  openModal(){
    if(this.type==1){
      this.currentCategory={id:1101,name:'Books'}
   }else{
      this.currentCategory={id:1102,name:'Video Games'}
   }
   if(this.selectedBook){
if(this.type==1){
this.newArticle.article.title=this.selectedBook.items[0].volumeInfo.title;
this.newArticle.article.author=this.selectedBook.items[0].volumeInfo.authors[0];
this.newArticle.article.description=this.selectedBook.items[0].volumeInfo.description;
}
}else{
  if(this.bookName){
    this.newArticle.article.title=this.bookName;
  }
  else{
  this.newArticle.article.title='Not Found';
  }
  this.newArticle.article.author='Not Found';
  this.newArticle.article.description='Not Found';
}
this.newArticle.article.spec.id=this.selectedlanguage.id
this.newArticle.article.category=this.currentCategory;
this.newArticle.article.profile=this.currentProfile;
this.newArticle.article.creationDate=new Date();
    let bookModal = this.modalCtrl.create('AddItemModalPage', { book: this.newArticle,profile:this.currentProfile });
    bookModal.present();
    

  }

 
  


  convertToDataURLviaCanvas(url, outputFormat){
    this.newArticle.type=outputFormat;
    return new Promise((resolve, reject) => {
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = () => {
      let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
        ctx = canvas.getContext('2d'),
        dataURL;
      canvas.height = this.imgHeight;
      canvas.width = this.imgWidth;
      ctx.drawImage(img, 0, 0);
      dataURL = canvas.toDataURL(outputFormat);
      resolve(dataURL);
      canvas = null;
    };
    img.src = url;
  });
}

  dismiss(){
    this.selectedlanguage =null;
    this.type=0;
    this.bookName=null;
    this.selectedBook=null;
    this.imagePath=null;

  this.navCtrl.parent.select(1, {}, {
      animate: true,
      direction: 'back'
    });
    
  }

  getInformations(){
    if(this.type==1){
    this.http.get('https://www.googleapis.com/books/v1/volumes?q='+this.bookName+'&maxResults=4&key=AIzaSyATWm5bqnC8yH7Zt7UzGHgVkTbJOEa1EIo').subscribe((res)=>{this.selectedBook=res.json()})
    this.getImageFromGoogle()
    }else{
      this.getVideo()
    }
  }


  getVideo(){
    this.imgHeight=1000;
    this.imgWidth=800;
    this.api.get( "GamesIgdb/"+this.bookName).subscribe(
      (res)=>{
        this.imagePath="https://images.igdb.com/igdb/image/upload/t_1080p/"+res.json()[0].cover.cloudinary_id+'.jpg'
        this.convertToDataURLviaCanvas('https://images.igdb.com/igdb/image/upload/t_1080p/'+res.json()[0].cover.cloudinary_id+'.jpg', 'jpg').then(base64 => {this.newArticle.file=base64;
          this.newArticle.file=this.newArticle.file.split(",")[1]});
          this.selectedBook=res.json();
          this.newArticle.article.averageRate=(res.json()[0].rating/20).toFixed(1);
          this.newArticle.article.title=res.json()[0].name;
          this.newArticle.article.description=res.json()[0].summary;
      })
  }

  getImageFromGoogle(){

    this.http.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyDknDRyJ09g-BbPOpue9_Ue4TyDlHkOCmE&cx=016301181575101414302:idcaho1v7ly&num=1&searchType=image&q='+this.bookName+' book').subscribe((res)=>{
      
    this.imagePath=res.json().items[0].link;
    this.imgHeight=res.json().items[0].image.height;
    this.imgWidth=res.json().items[0].image.width;
    this.convertToDataURLviaCanvas(this.imagePath, res.json().items[0].mime).then(base64 => {this.newArticle.file=base64;
    this.newArticle.file=this.newArticle.file.split(",")[1]})
  })



  }
  
  getLanguage(ev) {
     this.val = ev.target.value;
    if (!this.val || !this.val.trim()) {
      this.currentItems = [];
      return;
    }
    if(this.type==1){
    this.api.get("specsbycategory/1101/"+this.val).subscribe((res)=>this.currentItems=res.json())
    }else{
      this.api.get("specsbycategory/1102/"+this.val).subscribe((res)=>this.currentItems=res.json())
    }

  }
  selectBook(item){
  this.dismiss();
  let bookModal = this.modalCtrl.create('BookModalPage', { book: item ,profile:this.currentProfile});
    bookModal.present();

  }

  ChechIfExist(ev){
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.bookName=val;
    if(this.type==1){
    this.api.get("booksbytitle/"+val).subscribe((res)=>{this.availableBooks=res.json()})
    }else{
      this.api.get("booksbytitleandcategory/1102/"+val).subscribe((res)=>{this.availableBooks=res.json()})
    }
    }
  

  

  saveLanguage(item) {
    this.selectedlanguage=item;
  
  }


  ionViewDidLoad() {
    this.openModal2()
    this.principal.identity(true).then((account) => {
this.currentProfile=account.profile;      
  });
  }
  getPictureFromPhone(){
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 600,
        allowEdit: true,
        targetHeight: 1000,
        saveToPhotoAlbum:true
      }).then((fileUri) => {
        this.imagePath =  'data:image/PNG;base64,'+fileUri;
        this.newArticle.file=fileUri;
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }

  }
 
  getPicture() {
  
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 600,
        allowEdit: true,
        targetHeight: 1000,
        saveToPhotoAlbum:true
      }).then((fileUri) => {
        this.imagePath =  'data:image/PNG;base64,'+fileUri;
        this.newArticle.file=fileUri;
        
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  cropImage(url){
    return this.crop.crop(url, {quality: 75})
    .then(
      newImage => console.log('new image path is: ' + newImage),
      error => console.error('Error cropping image', error)
    );
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }



 




}



