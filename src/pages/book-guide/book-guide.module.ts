import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookGuidePage } from './book-guide';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    BookGuidePage,
  ],
  imports: [
    IonicPageModule.forChild(BookGuidePage),
    TranslateModule.forChild()
    
  ],
})
export class BookGuidePageModule {}
