import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {Storage} from "@ionic/storage";
/**
 * Generated class for the BookGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-guide',
  templateUrl: 'book-guide.html',
})
export class BookGuidePage {

  constructor(private storage: Storage,public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookGuidePage');
  }
  uderstand(){
    this.storage.set('isFirstTimeBookModal',false).then(()=>{
      this.viewCtrl.dismiss();
    })
     }

}
