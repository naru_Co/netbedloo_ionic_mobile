import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookModalPage } from './book-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    BookModalPage,
  ],
  imports: [
    IonicPageModule.forChild(BookModalPage),
    TranslateModule.forChild()
  ],
})
export class BookModalPageModule {}
