import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,Refresher,ModalController } from 'ionic-angular';
import {Api} from "../../providers/api/api"
import { Events } from 'ionic-angular';
import {Storage} from "@ionic/storage";
import { SocialSharing } from '@ionic-native/social-sharing';


/**
 * Generated class for the BookModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-modal',
  templateUrl: 'book-modal.html',
})
export class BookModalPage {
  article: any;
  numberPeopleWant:any;
  peopleWant:any;
  numberPeopleOwn:any;
  peopleOwn:any;
  currentProfile:any
  newArticle=0;
  own=false;
  want=false;
  theWant:any;
  theOwn:any;
  objectModifiedIndexOwn:any;
  objectModifiedIndexWant:any;
  constructor(public modalCtrl: ModalController,public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,private api: Api,public events: Events,private storage: Storage, private socialSharing: SocialSharing) {
    this.currentProfile=navParams.get('profile');
    console.log(this.currentProfile)
    
    this.article = navParams.get('book');
    this.storage.get('isFirstTimeBookModal').then(res=>{
      if (res!=false){
        this.openModal()
      }
    })
    if(!this.article){
this.newArticle=1;
    this.article = navParams.get('Newbook')
    }else{
      this.newArticle=0;
      
    }
  }

  openModal(){
    let bookModal = this.modalCtrl.create('BookGuidePage');
    bookModal.present();
  }
  

  getOwneds(id){
    this.api.get('ownedsbyarticle/'+id).subscribe((data)=>{
      this.peopleOwn=data.json();
      this.numberPeopleOwn= this.peopleOwn.length;
      this.checkIfOwn();
      

    })

  }
  compilemsg():string{
    var msg = this.currentProfile.firstName + "-" + this.article.title ;
    return msg.concat(" \n Sent from Netbedloo !");
  }

  
regularShare(){
  var msg = this.compilemsg();
  this.socialSharing.share(msg, null, null, null);
}

facebookShare(){
  var msg  = this.compilemsg();
   this.socialSharing.shareViaFacebook(msg, null, null);
 }

  getWanteds(id){
    this.api.get('wantedsbyarticle/'+id).subscribe((data)=>{
      this.peopleWant=data.json();
      this.numberPeopleWant=this.peopleWant.length;
      this.CheckIfWant();
    })
  }


  checkIfOwn(){
    console.log(this.currentProfile.id)
    this.objectModifiedIndexOwn = this.peopleOwn.findIndex(
      person => person.owner.id === this.currentProfile.id
    );
    if (this.objectModifiedIndexOwn !== -1) {
      this.theOwn=this.peopleOwn[this.objectModifiedIndexOwn];
      this.own=true;
    } else {
      this.own=false;
    }
  }


  CheckIfWant(){
    this.objectModifiedIndexWant = this.peopleWant.findIndex(
      person => person.wanter.id === this.currentProfile.id
    );
    if (this.objectModifiedIndexWant !== -1) {
      this.theWant=this.peopleWant[this.objectModifiedIndexWant];
      this.want=true;
    } else {
      this.want=false;
    }
    console.log(this.theWant)
  }

  AddOwn(){
    let obj={article:this.article,owner:this.currentProfile};
    this.own=true;
    
    this.api.post('owneds',obj).subscribe((res)=>{
      this.objectModifiedIndexOwn=this.numberPeopleOwn;
      this.numberPeopleOwn=this.numberPeopleOwn+1;
      this.numberPeopleOwn++;
      this.theOwn=res.json();
      this.peopleOwn.push(this.theOwn);
      this.events.publish('addingOwn')
    })
  }

  AddWant(){
    let obj={article:this.article,wanter:this.currentProfile};
    this.want=true;
    
        this.api.post('wanteds',obj).subscribe((res)=>{
          this.objectModifiedIndexWant=this.numberPeopleWant;
          this.numberPeopleWant++;
          this.theWant=res.json();
          this.peopleWant.push(this.theWant);
          this.events.publish('addingWant')

        })
  }

  DeleteOwn(){
    this.own=false;
    this.peopleOwn.splice(this.objectModifiedIndexOwn,1);
    this.numberPeopleOwn--;
    this.api.delete('owneds/'+this.theOwn.id).subscribe((res)=>{
      console.log(res);
      this.events.publish('addingOwn')
     
    })
  }
  DeleteWant(){
    this.want=false;
    this.peopleWant.splice(this.objectModifiedIndexWant,1);
    this.numberPeopleWant--;
    this.api.delete('wanteds/'+this.theWant.id).subscribe((res)=>{
      console.log(res);
      this.events.publish('addingWant')
   
    })
  }



  ionViewDidLoad() {
  
    this.getWanteds(this.article.id)
    this.getOwneds(this.article.id)
  }
  dismiss() {
    if(this.newArticle==1){
      this.navCtrl.setRoot('TabsPage', { }, {
        animate: true,
        direction: 'forward'
      });
    }else{
    this.viewCtrl.dismiss();
    }
  }


  doPulling(refresher: Refresher) {
 }


doRefresh(refresher: Refresher) {
  this.dismiss()
}

goToUser(user){ 
  if(user.id!=this.currentProfile.id){
    console.log(this.currentProfile);
    this.navCtrl.push('UserPage', {
    otherUser: user,currentUser:this.currentProfile
  }
  )
  }
 
}

}
