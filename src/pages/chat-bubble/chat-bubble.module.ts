import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatBubblePage } from './chat-bubble';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    ChatBubblePage,
  ],
  imports: [
    MomentModule,            
    IonicPageModule.forChild(ChatBubblePage),
  ],
})
export class ChatBubblePageModule {}
