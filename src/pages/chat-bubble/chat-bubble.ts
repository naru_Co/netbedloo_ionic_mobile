import {Component} from '@angular/core';

@Component({
  selector: 'chat-bubble',
  inputs: ['msg: message'],
  template:
  `
  <div class="chatBubble">
    <img class="profile-pic {{msg.position}}" src="https://www.netbedloo.com{{msg.thumbnailImage}}">
    <div class="chat-bubble {{msg.position}}">
      <div class="message">{{msg.content}}</div>
      <div class="message-detail">
          <span style="font-weight:bold;">{{msg.senderName}} </span>,
          <span>{{msg.creationDate | amTimeAgo}}</span>
      </div>
    </div>
  </div>
  `
})
export class ChatBubblePage {

  constructor() {
  }

  ionViewDidLoad() {
  }

}
