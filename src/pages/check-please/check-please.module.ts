import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckPleasePage } from './check-please';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CheckPleasePage,
  ],
  imports: [
    IonicPageModule.forChild(CheckPleasePage),
    TranslateModule.forChild()
    
  ],
})
export class CheckPleasePageModule {}
