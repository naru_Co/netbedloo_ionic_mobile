import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController,NavParams } from 'ionic-angular';

/**
 * Generated class for the CheckPleasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-check-please',
  templateUrl: 'check-please.html',
})
export class CheckPleasePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckPleasePage');
  }
  ok(){
    this.viewCtrl.dismiss();        
    
  }
  moveOn(){
    this.viewCtrl.dismiss();        
  }

}
