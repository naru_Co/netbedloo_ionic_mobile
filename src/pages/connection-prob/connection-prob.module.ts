import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectionProbPage } from './connection-prob';

@NgModule({
  declarations: [
    ConnectionProbPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectionProbPage),
  ],
})
export class ConnectionProbPageModule {}
