import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CameraServiceProvider } from '../../providers/camera-service/camera-service'

/**
 * Generated class for the CropImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crop-image',
  templateUrl: 'crop-image.html',
})
export class CropImagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public cameraService : CameraServiceProvider) {
    this.cameraService.getMedia()
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CropImagePage test test');
  }

}
