import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedDetailPage } from './feed-detail';
import { TranslateModule } from '@ngx-translate/core';
import {MomentModule} from 'angular2-moment';

@NgModule({
  declarations: [
    FeedDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedDetailPage),
    TranslateModule.forChild(),
    MomentModule
  ],
})
export class FeedDetailPageModule {}
