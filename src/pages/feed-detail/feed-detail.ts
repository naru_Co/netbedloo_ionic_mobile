import { Component } from '@angular/core';
import { ToastController, ActionSheetController, IonicPage, NavController, NavParams, ViewController, Refresher, ModalController } from 'ionic-angular';
import { Api } from "../../providers/api/api"
import { Events } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the BookModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feed-detail',
  templateUrl: 'feed-detail.html',
})
export class FeedDetailPage {
  feed: any;
  numberOfComments: any;
  numberLikes: any;
  liked: any;
  comments: any = [];
  comment: any
  profile: any
  page = -1;
  size = 5;
  currentItems: any = [];
  links: any;
  totalItems: number;
  commentPosted: string;
  wrong: string;
  deleteBut: string;
  cancelbut: string;
  constructor(public translateService: TranslateService,public toastCtrl: ToastController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private api: Api, public events: Events, private storage: Storage, private formBuilder: FormBuilder, public actionSheetCtrl: ActionSheetController) {
    this.feed = navParams.get('feed')
    this.profile = navParams.get('profile')

    this.comment = this.formBuilder.group({
      content: ['', Validators.compose([Validators.required, Validators.maxLength(1500)])]
    });
    this.events.subscribe('comment:added', (feed, comment) => {
      this.feed.numberOfComments++
      this.comments.unshift(comment);
    });
    translateService.get('DELETE_BUTTON').subscribe(
      value => {
        // value is our translated string
        this.deleteBut = value;
      })
    translateService.get('NEW_COMMENT_SUCC').subscribe(
      value => {
        // value is our translated string
        this.commentPosted = value;
      })
      translateService.get('WRONG').subscribe(
        value => {
          // value is our translated string
          this.wrong = value;
        })

  }
  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  abbrNum(number, decPlaces) {
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10, decPlaces);

    // Enumerate number abbreviations
    var abbrev = [" k", " m", " b", " t"];

    // Go through the array backwards, so we do the largest first
    for (var i = abbrev.length - 1; i >= 0; i--) {

      // Convert array index to "1000", "1000000", etc
      var size = Math.pow(10, (i + 1) * 3);

      // If the number is bigger or equal do the abbreviation
      if (size <= number) {
        // Here, we multiply by decPlaces, round, and then divide by decPlaces.
        // This gives us nice rounding to a particular decimal place.
        number = Math.round(number * decPlaces / size) / decPlaces;

        // Handle special case where we round up to the next abbreviation
        if ((number == 1000) && (i < abbrev.length - 1)) {
          number = 1;
          i++;
        }

        // Add the letter for the abbreviation
        number += abbrev[i];

        // We are done... stop
        break;
      }
    }

    return number;
  }
  ionViewDidLoad() {
    this.loadProps(this.feed)
    this.getComments()
  }
  openModal(article) {
    let bookModal = this.navCtrl.push('BookModalPage', { book: article, profile: this.profile });
    //bookModal.present();
  }
  getComments(infiniteScroll?: any) {
    this.page = this.page + 1;
    this.api.get('commentsoffeed/' + this.feed.id, {
      page: this.page,
      size: this.size,
      sort: ['id,desc']
    }).subscribe((res) => {
      this.onSuccess(res.json(), res.headers);
      if (infiniteScroll) {
        infiniteScroll.complete();
      }
    })

  }
  onSuccess(data, headers) {
    for (let i = 0; i < data.length; i++) {
      this.totalItems = headers.get('X-Total-Count');
      this.comments.push(data[i]);
    }
  }
  AddOkay() {
    let obj = { feed: this.feed, profile: this.profile };
    this.feed.account = true;
    this.feed.numberOfLikes++
    this.api.post('okays', obj).subscribe((res) => {


    })
  }
  dismiss() {

    this.viewCtrl.dismiss();

  }


  doPulling(refresher: Refresher) {
  }


  doRefresh(refresher: Refresher) {
    this.dismiss()
  }

  deleteOkay() {
    this.feed.account = false
    this.feed.numberOfLikes--
    this.api.get('okayprofileandfeed/' + this.feed.id + '/' + this.profile.id).subscribe((data) => {
      let comeOn = data.json()
      this.api.delete('okays/' + comeOn.id).subscribe((res) => {
      })


    })
  }
  deleteComment(comment) {
    this.feed.numberOfcomments--
    const index: number = this.comments.indexOf(comment);
    if (index !== -1) {
      this.comments.splice(index, 1); 
    } 
    
    this.api.delete('comments/' + comment.id).subscribe((res) => {
    })
  }
  async addComment(form: NgForm) {
    try {
      const query = await this.api.post('comments', {
        profile: this.profile,
        feed: this.feed,
        comment: this.comment.value.content
      }).subscribe((res) => {
        this.events.publish('comment:added', this.feed, res.json());
      })
      this.creationToast(this.commentPosted);
      this.comment.reset();
    } catch (err) {
      this.creationToast(this.wrong);
      this.comment.reset();


    }
  }
  presentActionSheet(feed, comment) {
    if (comment.profile.id === this.profile.id) {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Options',
        buttons: [
          {
            text: this.deleteBut,
            handler: () => {
              this.deleteComment(comment)
            }
          }
        ]
      });
      actionSheet.present();
    }
  }



  goToUser(user) {
    if (user.id != this.profile.id) {
      this.navCtrl.push('UserPage', {
        otherUser: user, currentUser: this.profile
      })
    }

  }
  getAgain(infiniteScroll) {
    console.log(this.totalItems+" =? "+this.page+" * "+this.size)
    if (this.totalItems > this.page * this.size) {
      this.getComments(infiniteScroll)
    } else {
      infiniteScroll.enable(false);
    }
  }
  loadProps(feed) {
    feed.account = false
    this.api.get('numberofcomments/' + feed.id).subscribe((data) => {
      var number = data.json()
      feed.numberOfcomments = this.abbrNum(number[0], 2);
    }, (res) => {
      feed.numberOfcomments = 0;
    })
    this.api.get('numberofokeys/' + feed.id).subscribe((data) => {
      var number2 = data.json()
      feed.numberOfLikes = this.abbrNum(number2[0], 2);
    }, (res) => {
      feed.numberOfLikes = 0;
    })
    this.api.get('okayprofileandfeed/' + feed.id + '/' + this.profile.id).subscribe((data) => {
      try {
        if (data.text()) {
          feed.account = true
        }
      } catch (e) {
        console.error(e)
      }
    })

  }
}