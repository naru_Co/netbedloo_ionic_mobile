import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedPage } from './feed';
import { MomentModule } from 'angular2-moment';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FeedPage,
  ],
  imports: [
    MomentModule,        
    IonicPageModule.forChild(FeedPage),
    TranslateModule.forChild()
    
  ],
})
export class FeedPageModule {}
