import { Component, ViewChild } from '@angular/core';
import { AlertController, ActionSheetController, IonicPage, NavController, NavParams, Refresher, Content } from 'ionic-angular';
import { Api } from "../../providers/api/api";
import { Events } from 'ionic-angular';
import { Principal } from "../../providers/providers";
import { TranslateService } from '@ngx-translate/core';


/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {
  @ViewChild(Content) content: Content;

  page = -1;
  size = 5;
  currentItems: any = [];
  links: any;
  totalItems: number;
  feeds: any = [];
  toggleB = 1;
  actualMessage: any;
  deleteTitle: string;
  deleteBody: string;
  deleteBut: string;
  cancelbut: string;
  loader: any;
  profile: any;
  okay: any;
  following: any = [];

  constructor(public translateService: TranslateService, public alertCtrl: AlertController, public events: Events, public navCtrl: NavController, public navParams: NavParams, private api: Api, private principal: Principal, public actionSheetCtrl: ActionSheetController) {

    this.events.subscribe('comment:added', (feed, comment) => {
      let selectedFeed = this.feeds.find(item => item.id === feed.id);
      this.loadProps(selectedFeed);
    });
    this.events.subscribe('feed:added', (feed) => {
      this.loadProps(feed);
      this.feeds.unshift(feed);
    });
    this.events.subscribe('load:error', () => {
      this.feeds = []
      this.getFeeds()
    });
    

    translateService.get('DELETE_BUTTON').subscribe(
      value => {
        // value is our translated string
        this.deleteBut = value;
      })
    translateService.get('CANCEL_BUTTON').subscribe(
      value => {
        // value is our translated string
        this.cancelbut = value;
      })
    translateService.get('DELETE_BODY').subscribe(
      value => {
        // value is our translated string
        this.deleteBody = value;
      })
    translateService.get('DELETE_HEAD').subscribe(
      value => {
        // value is our translated string
        this.deleteTitle = value;
      })
  }

  showConfirm(feed) {
    let confirm = this.alertCtrl.create({
      title: this.deleteTitle,
      message: this.deleteBody,
      buttons: [
        {
          text: this.cancelbut,
          handler: () => {

          }
        },
        {
          text: this.deleteBut,
          handler: () => {
            this.api.get('feeds/' + feed.id).subscribe((res) => {
              let updatedfeed = res.json()
              updatedfeed.statut = false;
              this.api.put('feeds', updatedfeed).subscribe((res) => {
                const index: number = this.feeds.indexOf(feed);
                if (index !== -1) {
                  this.feeds.splice(index, 1)
                }

              })
            })

          }
        }
      ]
    });
    confirm.present();
  }

  async ionViewDidLoad() {
    let account = await this.principal.identity(true)
    this.profile = account.profile;
    this.getFriendsList()
    this.getFeeds()

  }
  doRefresh(refresher: Refresher) {
    this.api.get('connectedfeeds' + this.profile.id, {
      page: 0,
      size: 3,
      sort: ['id,desc']
    }).subscribe((res) => {
      this.onSuccessReload(res.json(), res.headers);
      refresher.complete();
    }, (res) => {
      this.onErrorReload(refresher)
    })
  }

  private onSuccessReload(data, headers) {
    for (let i = 0; i < data.length; i++) {
      let selectedCategory = this.feeds.find(item => item.id === data[i].id);
      if (selectedCategory === undefined) {
        if (data[i].type === 1 || data[i].type === 2 || data[i].type === 4 || data[i].type === 5 || data[i].type === 6 || data[i].type === 8 || data[i].type === 9 || data[i].type === 10) {
          this.loadProps(data[i])
          this.feeds.unshift(data[i]);
        }

      }
    }
    for (let j = 0; j < this.feeds.length; j++) {
      this.loadProps(this.feeds[j])
    }
  }
  async followThisPerson(feed) {
    const query = await this.api.post('follows', {
      follower: this.profile,
      followed: feed.follow.followed,
    }).subscribe((res) => {
      feed.followEach = true
      this.following.push(feed.follow)
      this.events.publish('follow:added', res.json());
    }, (res) => {
    })
  }
  getFriendsList() {
    this.api.get("follows/" + this.profile.id).subscribe((res) => {
      this.following = res.json();
    }
    )
  }
  private onErrorReload(refresher) {
    this.events.publish('reload:error', refresher);
  }
  private onError(error) {
    
  }
  getFeeds(infiniteScroll?: any) {

    this.page = this.page + 1;
    this.api.get('connectedfeeds/' + this.profile.id, {
      page: this.page,
      size: this.size,
      sort: ['id,desc']
    }).subscribe((res) => {
      this.onSuccess(res.json(), res.headers);
      if (infiniteScroll) {
        infiniteScroll.complete();
      }
    }, (res) => {
      this.onError(res.json())
    })

  }
  getAgain(infiniteScroll) {

    if (this.totalItems > this.page * this.size) {

      this.getFeeds(infiniteScroll)
    } else {

      infiniteScroll.enable(false);
    }
  }

  loadProps(feed) {
    feed.account = false
    this.api.get('numberofcomments/' + feed.id).subscribe((data) => {
      var number = data.json()
      feed.numberOfcomments = this.abbrNum(number[0], 2);
    }, (res) => {
      feed.numberOfcomments = 0;
    })
    this.api.get('numberofokeys/' + feed.id).subscribe((data) => {
      var number2 = data.json()
      feed.numberOfLikes = this.abbrNum(number2[0], 2);
    }, (res) => {
      feed.numberOfLikes = 0;
    })
    this.api.get('okayprofileandfeed/' + feed.id + '/' + this.profile.id).subscribe((data) => {
      try {
        if (data.text()) {
          feed.account = true
        }
      } catch (e) {
        console.error(e)
      }

    })
    this.api.get('topcommentsfeed/' + feed.id).subscribe((data) => {
      feed.topComments = data.json()
    })
    if (feed.type === 10) {
      let followed = this.following.find(item => item.followed.id === feed.follow.followed.id);
      if (followed === undefined) {
        feed.followEach = false
      } else {
        feed.followEach = true
      }
    }
  }
  async talkTo(chatter) {
    let uidt = (chatter.id > this.profile.id) ? chatter.id + "naru" + this.profile.id : this.profile.id + "naru" + chatter.id;
    this.navCtrl.push('MessageDetailPage', { actualChatter: chatter, currentProfile: this.profile, uid: uidt, follow: true });
  }
  AddOkay(feed) {
    let obj = { feed: feed, profile: this.profile };
    feed.account = true;
    feed.numberOfLikes++
    this.api.post('okays', obj).subscribe((res) => {


    })
  }
  deleteOkay(feed) {
    feed.account = false
    feed.numberOfLikes--
    this.api.get('okayprofileandfeed/' + feed.id + '/' + this.profile.id).subscribe((data) => {
      let comeOn = data.json()
      this.api.delete('okays/' + comeOn.id).subscribe((res) => {
      })


    })
  }
  deleteComment(feed, comment) {
    const index: number = feed.topComments.indexOf(comment);
    if (index !== -1) {
      feed.topComments.splice(index, 1)
    }

    feed.numberOfcomments--
    this.api.delete('comments/' + comment.id).subscribe((res) => {
    })

  }
  abbrNum(number, decPlaces) {
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10, decPlaces);

    // Enumerate number abbreviations
    var abbrev = [" k", " m", " b", " t"];

    // Go through the array backwards, so we do the largest first
    for (var i = abbrev.length - 1; i >= 0; i--) {

      // Convert array index to "1000", "1000000", etc
      var size = Math.pow(10, (i + 1) * 3);

      // If the number is bigger or equal do the abbreviation
      if (size <= number) {
        // Here, we multiply by decPlaces, round, and then divide by decPlaces.
        // This gives us nice rounding to a particular decimal place.
        number = Math.round(number * decPlaces / size) / decPlaces;

        // Handle special case where we round up to the next abbreviation
        if ((number == 1000) && (i < abbrev.length - 1)) {
          number = 1;
          i++;
        }

        // Add the letter for the abbreviation
        number += abbrev[i];

        // We are done... stop
        break;
      }
    }

    return number;
  }

  openModal(article) {
    let bookModal = this.navCtrl.push('BookModalPage', { book: article, profile: this.profile });
    //bookModal.present();
  }
  newFeed() {
    let newFeed = this.navCtrl.push('AddFeedModalPage', { profile: this.profile });
    //bookModal.present();
  }

  openNewCommentModal(feed) {
    let addComment = this.navCtrl.push('AddCommentModalPage', { feed: feed, profile: this.profile });
    //bookModal.present();
  }
  goFeed(feed) {
    let opts: { animation: 'ios-transition' }
    this.navCtrl.push('FeedDetailPage', { feed: feed, profile: this.profile }, opts)
  }

  private async onSuccess(data, headers) {
    this.totalItems = await headers.get('X-Total-Count');
    for (let i = 0; i < data.length; i++) {
      if (data[i].type === 1 || data[i].type === 2 || data[i].type === 4 || data[i].type === 5 || data[i].type === 6 || data[i].type === 8 || data[i].type === 9 || data[i].type === 10) {
        this.loadProps(data[i])
        this.feeds.push(data[i]);
      }
    }
  }

  presentActionSheet(feed, comment) {
    if (comment.profile.id === this.profile.id) {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Options',
        buttons: [
          {
            text: this.deleteBut,
            handler: () => {
              this.deleteComment(feed, comment)
            }
          }
        ]
      });
      actionSheet.present();
    }
  }



  goToUser(user) {
    if (user.id != this.profile.id) {
      this.navCtrl.push('UserPage', {
        otherUser: user, currentUser: this.profile
      })
    }

  }


}
