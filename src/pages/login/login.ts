import {Component} from '@angular/core';
import {IonicPage,NavController, LoadingController, ToastController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {Principal} from "../../providers/providers"
import {Storage} from "@ionic/storage";
import {Api} from '../../providers/providers';
import {OneSignal} from '@ionic-native/onesignal';
import { AlertController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  playerId:any;
  authenticationError=false;
  
  constructor(private readonly navCtrl: NavController,
              private readonly loadingCtrl: LoadingController,
              private principal : Principal,
              private api:Api,
              private readonly authProvider: AuthProvider,
              private readonly toastCtrl: ToastController,
            private readonly storage : Storage,
            public alertCtrl: AlertController,
            private _OneSignal: OneSignal) {
              this._OneSignal.getIds().then(value=>{this.playerId=value.userId})
              
  }

  updateprofileWithPlayerId(profile){
    console.log
    profile.playerId=this.playerId;
    this.api.put('profiles',profile).subscribe()
    

  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Failed to sign in!',
      subTitle: 'Please check your credentials and try again.',
      buttons: ['OK']
    });
    alert.present();
  }

  theLogin(value){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Logging in ...'
    });

    loading.present();

    this.login(value).then(() => {
      loading.dismiss()
      this.authenticationError = false;
   
  }).catch(() => {
    loading.dismiss()
    this.showAlert();
      this.authenticationError = true;
  });
  }

  login(value, callback?) {
    value.rememberMe=true;
    
    const cb = callback || function() {};
    


    return new Promise((resolve, reject) => {
      this.authProvider.login(value)
      .subscribe((data) => {
          this.principal.identity(true).then((account) => {
              
              resolve(data);
             
          });
          return cb();
      }, (err) => {
          this.logout();
          reject(err);
          return cb(err);
      });
  });

 /*    this.authProvider
      .login(value)
      .subscribe(
        () => {this.navCtrl.push(CardsPage);},
        err => this.handleError(err)); */
  }

  handleError(error: any) {
    let message: string;
    if (error.status && error.status === 401) {
      message = 'Login failed';
    }
    else {
      message = `Unexpected error: ${error.statusText}`;
    }

    const toast = this.toastCtrl.create({
      message,
      duration: 5000,
      position: 'bottom'
    });

    toast.present();
  }

  logout() {
    this.authProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.storage.clear()
}

}