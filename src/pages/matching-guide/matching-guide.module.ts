import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchingGuidePage } from './matching-guide';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MatchingGuidePage,
  ],
  imports: [
    IonicPageModule.forChild(MatchingGuidePage),
    TranslateModule.forChild()
    
  ],
})
export class MatchingGuidePageModule {}
