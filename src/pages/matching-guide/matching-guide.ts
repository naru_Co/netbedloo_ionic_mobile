import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the MatchingGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-matching-guide',
  templateUrl: 'matching-guide.html',
})
export class MatchingGuidePage {

  constructor(private storage: Storage,public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MatchingGuidePage');
  }
  uderstand(){
    this.storage.set('isFirstTimeMatching',false).then(()=>{
      this.viewCtrl.dismiss();
    })
     }

}
