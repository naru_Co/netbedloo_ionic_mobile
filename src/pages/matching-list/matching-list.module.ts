import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchingListPage } from './matching-list';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MatchingListPage,
  ],
  imports: [
    IonicPageModule.forChild(MatchingListPage),
    TranslateModule.forChild()
    
  ],
})
export class MatchingListPageModule {
  
}
