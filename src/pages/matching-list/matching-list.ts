import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import {Api} from "../../providers/api/api";
import { ToastController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the MatchingListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-matching-list',
  templateUrl: 'matching-list.html',
})
export class MatchingListPage {
  matchings:any=[];
  toastConfirm:any;
  toastDelete:any;
  toastIndic:any;
  constructor(public modalCtrl: ModalController,private storage: Storage,public navCtrl: NavController, public navParams: NavParams,private api: Api,private toastCtrl: ToastController,public events: Events) {
    this.matchings=navParams.get('matching');
    
      this.storage.get('isFirstTimeMatching').then(res=>{
        if (res!=false){
          this.openModal()
        }
      })
    
    
  }
  //add inidication for swap and alert box in confirmation or decline
  ionViewDidLoad() {
    this.presentToast()
    
    }
    openModal(){
      let bookModal = this.modalCtrl.create('MatchingGuidePage');
      bookModal.present();
    }

  goMessage(match){
    let cuid=(match.me.id>match.other.id)?match.me.id+'naru'+match.other.id:match.other.id+'naru'+match.me.id;
    this.navCtrl.push('MessageDetailPage',{actualChatter:match.other,currentProfile:match.me,uid:cuid})

  }
  confirmMatching(match){
    console.log(match)
    this.api.get('matcheds/'+match.id).subscribe(realMatch=>{
      let confirmedMatch=realMatch.json();
      confirmedMatch.statut=true;
      console.log(confirmedMatch);
      this.api.put('matcheds/',confirmedMatch).subscribe(resp=>{ 
      },err=>{console.log(err)});
    },(error=>{}));
    this.toastConfirm.present();
    this.matchings = this.matchings.filter((item) => {
      return item.id !==match.id;
    })
    this.events.publish('UpdatedMatching', this.matchings);

    
    
  }
  declineMatching(match){
      this.api.delete('matcheds/'+match.id).subscribe(resp=>{ this.toastDelete.present();
        this.matchings = this.matchings.filter((item) => {
          return item.id !==match.id;
        })
        this.events.publish('UpdatedMatching', this.matchings);
      },err=>{console.log(err)});
     
  }

  presentToast() {
    this.toastConfirm = this.toastCtrl.create({
      message: 'matching Confirmed',
      duration: 3000,
      position: 'bottom'
    });

    this.toastDelete = this.toastCtrl.create({
      message: 'matching Deleted',
      duration: 3000,
      position: 'bottom'
    });

    this.toastIndic = this.toastCtrl.create({
      message: 'Swipe left or right to select options',
      duration: 3000,
      position: 'bottom',
      showCloseButton :true,
    });
   
  
   
  
  
  }

}
