import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchingModalPage } from './matching-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MatchingModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MatchingModalPage),
    TranslateModule.forChild()
    
  ],
})
export class MatchingModalPageModule {}
