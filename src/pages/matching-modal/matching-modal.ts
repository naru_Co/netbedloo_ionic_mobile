import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,Refresher } from 'ionic-angular';

/**
 * Generated class for the MatchingModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-matching-modal',
  templateUrl: 'matching-modal.html',
})
export class MatchingModalPage {
  matchinginfo:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
    this.matchinginfo = navParams.get('matching');

  }

  ionViewDidLoad() {
  }

  goToMessage(){
    let uidt =(this.matchinginfo.other.id>this.matchinginfo.me.id)? this.matchinginfo.other.id+"naru"+this.matchinginfo.me.id : this.matchinginfo.me.id+"naru"+this.matchinginfo.other.id;
    this.navCtrl.push('MessageDetailPage',{actualChatter : this.matchinginfo.other,currentProfile:this.matchinginfo.me,uid:uidt});
    
  }

  dismiss() {
  
    this.viewCtrl.dismiss();
    
  }


  doPulling(refresher: Refresher) {
 }


doRefresh(refresher: Refresher) {
  this.dismiss()
}

}
