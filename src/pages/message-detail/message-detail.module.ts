import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageDetailPage } from './message-detail';
import { MomentModule } from 'angular2-moment';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MessageDetailPage,
  ],
  imports: [
    MomentModule,
    IonicPageModule.forChild(MessageDetailPage),
    TranslateModule.forChild()
  ],
})
export class MessageDetailPageModule {}
