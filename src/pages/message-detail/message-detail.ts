import { Component, ViewChild } from '@angular/core';
import {Events, ToastController,  LoadingController,IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from "../../providers/api/api";
import { Content } from 'ionic-angular';
import { PrivateChatProvider } from '../../providers/private-chat/private-chat';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the MessageDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message-detail',
  templateUrl: 'message-detail.html',
})
export class MessageDetailPage {
  @ViewChild(Content) content: Content;
  @ViewChild("sendInput") sendInput;

  profile: any;
  chatter: any;
  followed: any;
  page = -1;
  size = 10;
  uid = "";
  currentItems: any = [];
  links: any;
  loading: any;
  saving:string;
  gotcha:boolean;
  totalItems: number;
  messageArray: any = [];
  toggleB = 1;
  actualMessage: any;
  waitingMessage: any;
  loader: any;
  constructor(public events: Events,public toastCtrl: ToastController,public loadingCtrl: LoadingController,public translateService: TranslateService,public navCtrl: NavController, public navParams: NavParams, private api: Api, private privateMessage: PrivateChatProvider) {
    this.chatter = navParams.get('actualChatter');
    this.profile = navParams.get('currentProfile');
    this.uid = navParams.get('uid');
    this.followed = navParams.get('follow');
    console.log(this.followed);
    this.waitingMessage = {
      sender: this.profile,
      receiver: this.chatter,
      uid: this.uid,
      content: ''
    }
    translateService.get('SAVING').subscribe(
      value => {
        // value is our translated string
        this.saving = value;
      })
      translateService.get('GOTCHA').subscribe(
        value => {
          // value is our translated string
          this.gotcha = value;
        })
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content:  this.saving,
    });

  }

  ionViewDidLoad() {

    this.getfirstTime()
    this.seeTheUnseen();


  }

  seeTheUnseen() {
    this.api.get('see-private-messages/' + this.uid + '/' + this.profile.id).subscribe();
  }

  getInstantMessage() {
    this.privateMessage.unsubscribe()
    let ids = this.uid.split('naru');
    this.privateMessage.connect(ids[0], ids[1]);
    this.privateMessage.receive().subscribe((notif) => {
      if (notif.receiver.id === this.profile.id) {
        this.api.put('messages/', notif).subscribe();
      }
      notif.position = (notif.sender.id === this.profile.id) ? "right" : "left";

      this.messageArray.push(notif);
      setTimeout(() => {
        this.content.scrollToBottom();
      });
    })
  }

  sendMessage() {

    if (this.actualMessage.length > 0) {
      this.waitingMessage.content = this.actualMessage;
      let ids = this.uid.split('naru');
      this.privateMessage.sendMessage(this.waitingMessage, ids[0], ids[1]);
      this.events.publish('message:added',this.chatter);
      this.actualMessage = '';
      this.sendInput.setFocus();

    }
  }

  getfirstTime() {
    this.loader = true;
    this.page++;
    this.api.get('private-messages/' + this.uid, {
      sort: ['id,desc'],
      page: this.page,
      size: this.size,
    }).subscribe((res) => {
      this.totalItems = Number(res.headers.get('X-Total-Count'));
      this.messageArray = res.json();
      this.messageArray.reverse();
      this.loader = false;

      for (let i = 0; i < this.messageArray.length; i++) {
        if (this.messageArray[i].receiver.id === this.profile.id) {
          this.api.put('messages/', this.messageArray[i]).subscribe();
        }
        this.messageArray[i].position = (this.messageArray[i].sender.id === this.profile.id) ? "right" : "left";
      }
      this.getInstantMessage();
      setTimeout(() => {
        this.content.scrollToBottom();
      });
    }, (res) => {
      this.onError(res.json)
    });
  }
  async followThisPerson() {
    const query = await this.api.post('follows', {
      follower: this.profile,
      followed: this.chatter,
    }).subscribe((res) => {
      this.loading.dismiss();
      this.events.publish('follow:added',res.json());
      this.creationToast(this.gotcha);
      this.followed=true;
    }, (res) => {
      this.loading.dismiss();
    })
  }
  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  getMessages(infiniteScroll?: any) {
    if (this.totalItems > this.page * this.size) {
      this.page++;
      this.loader = true;

      this.api.get('private-messages/' + this.uid, {
        sort: ['id,desc'],
        page: this.page,
        size: this.size,
      }).subscribe((res) => {
        this.onSuccess(res.json(), res.headers);

        if (infiniteScroll) {
          infiniteScroll.complete();

        }
      },
        (res) => {
          this.onError(res.json);
        })
    } else {
      infiniteScroll.enable(false);
    }

  }

  private onSuccess(data, headers) {
    this.loader = false;

    for (let i = 0; i < data.length; i++) {
      data[i].position = (data[i].sender.id === this.profile.id) ? "right" : "left";
      this.messageArray.unshift(data[i]);

    }
  }


  private onError(error) {
    this.loader = false;
  }

  goToUser(user) {
    if (user.id != this.profile.id) {
      this.navCtrl.push('UserPage', {
        otherUser: user, currentUser: this.profile
      }
      )
    }

  }


}
