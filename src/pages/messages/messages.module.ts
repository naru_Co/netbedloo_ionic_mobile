import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesPage } from './messages';
import { MomentModule } from 'angular2-moment';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MessagesPage,
  ],
  imports: [
    MomentModule,        
    IonicPageModule.forChild(MessagesPage),
    TranslateModule.forChild()
    
  ],
})
export class MessagesPageModule {}
