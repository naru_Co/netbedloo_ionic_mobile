import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from "../../providers/api/api";
import { Events } from 'ionic-angular';

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  profile: any;
  waitingChatter: any;
  following: any = [];
  followers: any;
  souhabi: any;
  followEach: boolean;
  listLoading: boolean;
  actifSouhabi: any = [];
  currentItems: any = [];
  valeur: any;
  lastM: any;
  FiltredcurrentItems: any = [];
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams, private api: Api) {
    this.profile = navParams.get('currentProfile');
    this.actifSouhabi = navParams.get('waitingChatter');
    this.actifSouhabi.reverse()
    this.events.subscribe('follow:added', (follow) => {
      this.following.push(follow)

    });
    this.events.subscribe('message:added', async (chatter) => {
      let uidt = (chatter.id > this.profile.id) ? chatter.id + "naru" + this.profile.id : this.profile.id + "naru" + chatter.id;
      let lastMessage =  this.api.get(/lastMessage/ + uidt).subscribe((res) => {
        this.lastM =  res.json()
        let waitingIndex = this.actifSouhabi.findIndex(stored => stored.id === this.lastM.receiver.id);
        if (waitingIndex === -1) {
          let newSouhayb: any;
          newSouhayb = this.lastM.receiver;
           newSouhayb.message =  this.lastM;
           this.actifSouhabi.unshift(newSouhayb)
        } else {
           this.actifSouhabi[waitingIndex].message =  this.lastM;
        }
      }
      )


    });
    this.getFriendsList()
  }

  ionViewDidLoad() {


  }
  getPeople(ev) {
    let val = ev.target.value;
    this.valeur = val;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.api.get("profilesbypseudo/" + val).subscribe((res) => this.currentItems = res.json())
  }

  getFriendsList() {
    this.api.get("follows/" + this.profile.id).subscribe((res) => {
      this.following = res.json();
    }
    )
  }

  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.FiltredcurrentItems = [];
      return;
    }

    this.api.get("profilesbypseudo/" + val).subscribe((res) => this.currentItems = res.json())


  }

  async goToMessage(chatter) {
    let followed = this.following.find(item => item.followed.id === chatter.id);
    if (followed === undefined) {
      this.followEach = await false
    } else {
      this.followEach = await true
    }
    let uidt = (chatter.id > this.profile.id) ? chatter.id + "naru" + this.profile.id : this.profile.id + "naru" + chatter.id;
    this.navCtrl.push('MessageDetailPage', { actualChatter: chatter, currentProfile: this.profile, uid: uidt, follow: this.followEach });

  }



}
