import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileGuidePage } from './profile-guide';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ProfileGuidePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileGuidePage),
    TranslateModule.forChild()
    
  ],
})
export class ProfileGuidePageModule {}
