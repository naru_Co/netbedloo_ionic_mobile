import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {Storage} from "@ionic/storage";
/**
 * Generated class for the ProfileGuidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-guide',
  templateUrl: 'profile-guide.html',
})
export class ProfileGuidePage {

  constructor(private storage: Storage,public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileGuidePage');
  }
  uderstand(){
    this.storage.set('isFirstTimeProfile',false).then(()=>{
      this.viewCtrl.dismiss();
    })
     }

}
