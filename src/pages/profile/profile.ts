import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,Refresher } from 'ionic-angular';
import {Principal} from "../../providers/providers"
import {Storage} from "@ionic/storage";
import {Api} from "../../providers/api/api"
import { ActionSheetController } from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import { Events } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';

import { CameraServiceProvider } from '../../providers/camera-service/camera-service';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  profile : any;
  numberOwneds:Number;
  numberWanteds:Number;
  OwnedsItems:any;
  WantedsItems:any;
  ItemsToShow : any;
  choice:string="own";
  isOwn:any;
  me:any;
  newprofile:any={file:null,profile:null};
  loader:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private principal : Principal,private storage : Storage,private api: Api,public modalCtrl: ModalController,public actionSheetCtrl: ActionSheetController,private readonly authProvider: AuthProvider,public events: Events,public cameraServiceProvider : CameraServiceProvider,public loadingCtrl: LoadingController,private geolocation: Geolocation,private nativeGeocoder: NativeGeocoder) {
    this.events.subscribe('addingOwn', () => {
      this.principal.identity(true).then((account) => {  
        console.log(account)
        this.profile=account.profile;
        this.getOwneds(this.profile.id);        
    })
      
    });
    this.events.subscribe('following',()=> {

    });
    this.events.subscribe('addingWant', () => {
      this.principal.identity(true).then((account) => {  
        this.profile=account.profile;
        this.getWanteds(this.profile.id);        
    })
      
    });
    this.presentLoading()

  }
  //update shelf and whishlist from events from books modal
  logout() {
    this.profile.playerId=null;
    this.api.put('profiles',this.profile).subscribe(()=>{
      this.authProvider.logout().subscribe(()=>{
        
        this.principal.authenticate(null);
        this.storage.clear().then(()=>{
          
          this.navCtrl.setRoot('TutorialPage').then(()=>{
            this.navCtrl.popToRoot();
          })
          
  
        })
  
      }
    );

    })
}
getcurrentPos(){
  this.geolocation.getCurrentPosition().then((resp) => {
    console.log(resp)
    this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
    .then((result: NativeGeocoderReverseResult) => {
      let gouv={longitude:null,latitude:null,name:null};
      gouv.latitude=resp.coords.latitude;
      gouv.longitude=resp.coords.longitude;
      if(result.subLocality){
        gouv.name=result.subLocality+" / "+this.profile.id+" / "+this.profile.pseudo;
    }else{
      gouv.name=result.locality+" / "+this.profile.id+" / "+this.profile.pseudo;
    }
    this.updateLocalization(gouv);

  }).catch((error: any) => console.log('wtfffffffffff'));
   }).catch((error) => {
     console.log('Error getting location', error);
   });
}

updateLocalization(gouv){
  gouv.country=this.profile.country;
  this.api.post('gouvs',gouv).subscribe((res)=>{
    this.profile.gouv=res.json();
    this.loader.present()
    this.api.put('profiles',this.profile).subscribe((data)=>{
      this.profile=data.json();
      this.loader.dismiss();      
    },err=>{
      this.loader.dismiss();            
    })
  })
 
}
presentLoading() {
  this.loader = this.loadingCtrl.create({
    content: "Please wait...",
  });
}
getPicture(){
  this.cameraServiceProvider.getMedia().then(base64=>{
    this.newprofile.file=base64.split(",")[1];
    this.newprofile.profile=this.profile;
    this.loader.present()
    this.api.post('profiles',this.newprofile).subscribe((data)=>{
      this.profile=data.json();
      this.loader.dismiss();      
    },err=>{
      this.loader.dismiss();            
    })
  },err=>{

  })

}

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [
        {
          text: 'Logout',
          handler: () => {
          this.logout() 
              }
        },
        {
          text:'Localize me',
          handler: ()=>{
            this.getcurrentPos()
          }
        },
        {
          text: 'Change my picture',
          handler: ()=>{
            this.getPicture()
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  getOwneds(id){

    this.api.get('owneds/'+id).subscribe((data)=>{
      this.numberOwneds=data.json().length;
      this.OwnedsItems=data.json();
      this.ItemsToShow=data.json();

    })
    
  }

  openModal(article){
    let bookModal =this.navCtrl.push('BookModalPage', { book: article,profile:this.profile });
    

  }

  getWanteds(id){
    
        this.api.get('wanteds/'+id).subscribe((data)=>{
          this.numberWanteds=data.json().length;
          this.WantedsItems=data.json();
    
        })
        
      }

      showType(type:Number){
        if (type ===1){
        this.ItemsToShow=this.OwnedsItems;
        } else {
          this.ItemsToShow=this.WantedsItems;
        }
      }


  doPulling(refresher: Refresher) {
  }
 
 
 doRefresh(refresher: Refresher) {
  this.principal.identity(true).then((account) => {  
    refresher.complete();
    
    this.profile=account.profile;
    this.getOwneds(this.profile.id);
    this.getWanteds(this.profile.id);   
    
})
 }
 openModal2(){
  let bookModal = this.modalCtrl.create('ProfileGuidePage');
  bookModal.present();
}

  ionViewDidLoad() {
    this.isOwn=true;
    this.storage.get('userInformation').then((account) => {
      this.storage.get('isFirstTimeProfile').then(res=>{
        if (res!=false){
          this.openModal2()
        }
      })
      if(JSON.parse(account)==null){
     
            this.principal.identity(true).then((account) => {
          
              this.profile=account.profile;
              this.getOwneds(this.profile.id);
              this.getWanteds(this.profile.id); 
              
                
          })
          
      }else{
      let acountTemp=JSON.parse(account);
      let temp = this.navParams.get('otherUser');
      this.profile=acountTemp.profile;
      this.getOwneds(this.profile.id);
      this.getWanteds(this.profile.id); 
      





    }
    })

   }



}
