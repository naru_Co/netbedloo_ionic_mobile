import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import {Storage} from "@ionic/storage";

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';
import {Api} from "../../providers/api/api";
import {Principal} from "../../providers/providers"

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  currentItems: any;
  itemsPerPage = 18;
  links: any;
  page: any =-1;
  totalItems: number;
  books:any = [];
  toggleB=1;
  profile:any;
  valeur:any;

  constructor(public storage:Storage,public navCtrl: NavController, public navParams: NavParams, public items: Items,private api: Api,public modalCtrl: ModalController,private principal : Principal) { 
 
  }

  /**
   * Perform a service for the proper items.
   */
  getAgain(infiniteScroll){
    if (this.totalItems>this.page*this.itemsPerPage){
    this.getAllBooks(infiniteScroll)
    }else{
      infiniteScroll.enable(false);
    }
  }
  swipe(){
    this.toggleB=1;
  }
  goToAdd(){
    let bookModal = this.navCtrl.push('AdditemPage', {profile:this.profile });
    
  }
  getAllBooks(infiniteScroll?:any) {
    this.page=this.page+1;
    this.api.get('articlesbycategory/1101',{
      page: this.page,
      size: this.itemsPerPage,
      sort:['popularity,desc']
  }).subscribe((res) => {this.onSuccess(res.json(), res.headers);
  if(infiniteScroll){
    infiniteScroll.complete();
  }},
  (res) => this.onError(res.json))
  
  
  }
  openModal(article){
    let bookModal = this.navCtrl.push('BookModalPage', { book: article,profile:this.profile });
    //bookModal.present();
    

  }
   private onSuccess(data, headers) {
    this.totalItems = headers.get('X-Total-Count');
    for (let i = 0; i < data.length; i++) {
        this.books.push(data[i]);
    }
  }


  private onError(error) {
  
}

  ionViewDidLoad() {
      this.principal.identity(true).then((account) => {  
        
        this.profile=account.profile;
    
        
    })
     
    this.getAllBooks()
  }



  getItems(ev) {
    let val = ev.target.value;
    this.valeur=val;
    console.log(this.valeur)
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.api.get("booksbytitleandcategory/1101/"+val).subscribe((res)=>this.currentItems=res.json())
    

  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

}
