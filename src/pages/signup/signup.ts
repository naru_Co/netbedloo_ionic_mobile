import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController} from 'ionic-angular';
import {Http} from "@angular/http";
import { User } from '../../providers/providers';
import { MainPage } from '../pages';
import {CountriesProvider} from '../../providers/providers';
import {LocalsProvider} from '../../providers/providers';
import {Country} from '../../models/country';
import {Component, ViewChild} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {NgModel} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import {Api} from '../../providers/providers';
import { CameraServiceProvider } from '../../providers/camera-service/camera-service';
import { AlertController } from 'ionic-angular';
import {OneSignal} from '@ionic-native/onesignal';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  @ViewChild('fileInput') fileInput;
  usernameModel: NgModel;
  newprofile:any;
  account: any;
  repassword:any;
  location: string ='';
  defaultImage : string = 'assets/img/user_pic.png';
  currentCountries : Country[];
  success: boolean;
  doNotMatch: string;
  error: string;
  errorEmailExists: string;
  errorUserExists: string;
  loading:any;
  lang:any;
  playerId:any;
  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public countries : CountriesProvider,
    private readonly locals : LocalsProvider,
    private readonly loadingCtrl: LoadingController,
    private readonly http: Http,
    public camera: Camera,
    public cameraServiceProvider : CameraServiceProvider,
    private api:Api,
    public alertCtrl: AlertController,
    private _OneSignal: OneSignal ) {
      this.lang=navigator.language.split('-')[0];
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    this.countries.query().subscribe((value)=>{
      this.currentCountries=value;
    })

    this.newprofile={
      file :null,
      name:null,
      type:null,
      profile:{},
    };

    this.account= {
      login:'',
      firstName: '',
      lastName:'',
      email: '',
      langKey: '',
      password:'',
      profile:{}
    };
    this.success = false;

    this._OneSignal.getIds().then(value=>{this.playerId=value.userId})
    

 
    
  }

  saveLocal(){
    this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+this.location+'&components=country:'+this.newprofile.profile.country.code + '&key=AIzaSyDxX74bLnGn7AxGxH4HM7pM5Yzul2VH8Zc').subscribe((data)=>{this.extractInfo(data)});
  }

  extractInfo(data){
    let newgov={latitude:'',longitude:'',name:'',country:{}};
    newgov.latitude=data.json().results[0].geometry.location.lat
    newgov.longitude=data.json().results[0].geometry.location.lng
    newgov.name=data.json().results[0].address_components[0].long_name
    newgov.country=this.newprofile.profile.country;
    this.locals.get(newgov.name).subscribe((res)=>{
      this.newprofile.profile.gouv=res
      this.saveProfile();
    }, err => this.notfoundGouv(newgov))
  }


  saveProfile(){


    this.loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Signing up ...'
    });
    this.doNotMatch = null;
    this.error = null;
    this.errorUserExists = null;
    this.errorEmailExists = null;
    this.newprofile.type="jpg";
    this.account.firstName=this.newprofile.profile.firstName;
    this.account.lastName=this.newprofile.profile.lastName;
    this.account.langKey=this.lang;
    this.newprofile.profile.email=this.account.email;
    this.loading.present();
    this.api.post('profiles',this.newprofile).subscribe((data)=>{
      this.account.profile=data.json();
      this.saveAccount()},(err)=> {this.loading.dismiss(); this.processError(err)});
    
  }


  saveAccount(){
    this.api.post('register',this.account)
    .finally(() => this.loading.dismiss())
    .subscribe(()=>{
      this.success=true;
      this.empty();
      this.navCtrl.push('CheckMailPage');
    },(response)=>{
      this.processError(response)
    }
   
    );
  }


empty(){

this.newprofile={
      file :'',
      name:'',
      type:'',
      profile:null,
    };

    this.account= {
      login:'',
      firstName: '',
      lastName:'',
      email: '',
      langKey: '',
      password:'',
      profile:{}
    };
    this.success = false;
    this.repassword=null;
}




  signup() {
    if (this.account.password !== this.repassword) {
      this.doNotMatch = 'ERROR';
  } else {
  
    this.saveLocal()
    //this.saveProfile()
    }
  }


  private processError(response) {
    this.success = null;
    if (response.status === 400 && response._body === 'login already in use') {
        this.errorUserExists = 'ERROR';
        this.showAlert('Login name already registered!','Please choose another one.')

    } else if (response.status === 400 && response._body === 'email address already in use') {
        this.errorEmailExists = 'ERROR';
        this.showAlert('Email is already in use!','Please choose another one.')
    } else {
        this.error = 'ERROR';
        this.showAlert('Registration failed!','Please try again later.')
    }
  }

  showAlert(errorH,errorB) {
    let alert = this.alertCtrl.create({
      title: errorH,
      subTitle: errorB,
      buttons: ['OK']
    });
    alert.present();
  }
  
 




  notfoundGouv(data: any) {
  
    this.locals.save(data).subscribe((res)=>{
      this.newprofile.profile.gouv=res;
      this.saveProfile()})

  }


  
  handleError(error: any) {
    let message = `Unexpected error occurred`;
    const toast = this.toastCtrl.create({
      message,
      duration: 5000,
      position: 'bottom'
    });

    toast.present();
  }
  getPicture(){
    this.cameraServiceProvider.getMedia().then(base64=>{
      this.newprofile.file=base64.split(",")[1];


    })

  }


  getPicture2() {
    
      if (Camera['installed']()) {
        this.camera.getPicture({
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
          
          targetWidth: 600,
          targetHeight: 600,
          saveToPhotoAlbum:true
        }).then((fileUri) => {
          this.newprofile.file =  fileUri;
          
        }, (err) => {
          alert('Unable to take photo');
        })
      } else {
        this.fileInput.nativeElement.click();
      }
    }


  convertToDataURLviaCanvas(url, outputFormat){
    return new Promise((resolve, reject) => {
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = () => {
      let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
        ctx = canvas.getContext('2d'),
        dataURL;
      canvas.height = 1000;
      canvas.width = 1000;
      ctx.drawImage(img, 0, 0);
      this.newprofile.file = canvas.toDataURL(outputFormat);
      resolve(dataURL);
      canvas = null;
    };
    img.src = url;
  });
}



  processWebImage(event) {
    console.log(event);
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.newprofile.file = imageData;
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url('+ 'data:image/PNG;base64,'+ this.newprofile.file + ')'
  }




  doSignup() {
    // Attempt to login in through our User service
    this.user.signup(this.account).subscribe((resp) => {
      this.navCtrl.push(MainPage);
    }, (err) => {

      this.navCtrl.push(MainPage);

      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
