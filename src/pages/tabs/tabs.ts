import { Component,ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import {Storage} from "@ionic/storage";
import {Principal} from "../../providers/providers"
import { Events } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';

import { Tab1Root } from '../pages';
import { Tab2Root } from '../pages';
import { Tab3Root } from '../pages';
import { Tab4Root } from '../pages';
import { BackgroundMode } from '@ionic-native/background-mode';

import {PrivateChatProvider} from '../../providers/private-chat/private-chat'
import { ChatNotifProvider } from '../../providers/private-chat/chat-notif';
import { MatchiNotifProvider } from '../../providers/matchi-notif/matchi-notif';
import {OneSignal} from '@ionic-native/onesignal';

import {Api} from "../../providers/api/api";
import * as _ from 'lodash';
import { Observable } from 'rxjs/Rx';
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;
  tab4Root: any = Tab4Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";
  tab4Title = " ";
  profile:any;
  chatters:any=[];
  matchings:any=[];
  following:any;
  followers:any;
  souhabi:any;
  listLoading:boolean;
  actifSouhabi:any=[];
  playerId:any;
  
  constructor(public navCtrl: NavController, public translateService: TranslateService,private privateMessage : PrivateChatProvider,private principal : Principal,private storage : Storage,private chatNotifProvider : ChatNotifProvider,private matchingNotif : MatchiNotifProvider,private api: Api,public events: Events,private _OneSignal: OneSignal,private localNotifications: LocalNotifications,private backgroundMode: BackgroundMode) {
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE', 'TAB4_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
      this.tab4Title = values['TAB4_TITLE'];
    });

    this.events.subscribe('UpdatedMatching', (UpdatedMatchings) => {
      this.matchings=UpdatedMatchings;
    });
    this.listLoading=true;
    this.matchings=[];
    this.chatters=[];
    //this.backgroundMode.enable();
    
    this.localNotifications.on('click', (notification, state) => {
      console.log(notification);
    this.goToMessage(notification.id) });
  
  
   
  }

  goToMessage(id){
    let chatter = this.actifSouhabi.filter(souhayb=>{ return souhayb.id===id })[0]; 
    
    let uidt =(chatter.id>this.profile.id)? chatter.id+"naru"+this.profile.id : this.profile.id+"naru"+chatter.id;
    this.navCtrl.push('MessageDetailPage',{actualChatter : chatter,currentProfile:this.profile,uid:uidt});
    
  }

  //if a new message came from an unknown person, add it to the souhabi and actif souhabi list

  ionViewDidLoad() {
    this.storage.get('userInformation').then((account) => {
      if(JSON.parse(account)==null){
        
            this.principal.identity(true).then((account) => { 
              this.profile=account.profile; 
              console.log(this.profile)
              this.updateprofileWithPlayerId(this.profile)
              this.getListOfChatters();
              this.getAllMatching();
            })
      }else{
      this.profile=JSON.parse(account).profile;
      this.updateprofileWithPlayerId(this.profile)
      
      this.getListOfChatters();
      this.getAllMatching();
      
      
     
    }
    })

   }


   updateprofileWithPlayerId(profile){
    this._OneSignal.getIds().then(value=>{this.playerId=value.userId
      profile.playerId=this.playerId;
      this.api.put('profiles',profile).subscribe()
    })
    
    

  }
  

   connectAndReceive(id){
    this.matchingNotif.connect(id);
    this.matchingNotif.receive().subscribe((notif)=>{
      notif.me=this.profile;
      (notif.owner.id===this.profile.id)?(notif.other=notif.wanter,notif.mine=notif.owned,notif.their=notif.wanted):(notif.other=notif.owner,notif.mine=notif.wanted,notif.their=notif.owned);
      notif.distance=notif.distance.toFixed(1);
      delete notif.wanter;
      delete notif.wanted;
      delete notif.owned;
      delete notif.owner;
      this.navCtrl.push('MatchingModalPage',{matching : notif})
      
      this.playSuccessNotifAudio()
      this.matchings.push(notif);
    })
    this.chatNotifProvider.unsubscribe()
    this.chatNotifProvider.connect(id);
    this.chatNotifProvider.receive().subscribe((notif)=>{
      this.organizeChatNotifs(notif)
    })
   }

   getAllMatching(){
     this.api.get('matchedsByProfile/'+this.profile.id).subscribe((data)=>{
       data.json().map(notif=>{
        notif.me=this.profile;
        (notif.owner.id===this.profile.id)?(notif.other=notif.wanter,notif.mine=notif.owned,notif.their=notif.wanted):(notif.other=notif.owner,notif.mine=notif.wanted,notif.their=notif.owned);
        notif.distance=notif.distance.toFixed(1);
        delete notif.wanter;
        delete notif.wanted;
        delete notif.owned;
        delete notif.owner;
        this.matchings.push(notif);
        
       })
     })
   }

   playNotifAudio(){
    let audio = new Audio('assets/audio/notif.mp3');
    audio.play();
   }
   playSuccessNotifAudio() {
    let audio = new Audio('assets/audio/Success.mp3');
    audio.play();
  };

  organizeChatNotifs(message){
    let chatterIndex = this.chatters.findIndex(
      stored => stored.sender.id === message.sender.id
    );

    this.localNotifications.schedule({
      id: message.sender.id,
      title: "Netbedloo - "+message.sender.firstName +' '+message.sender.lastName,
      text: message.content.slice(0, 15)+'...',
      icon: "https://www.netbedloo.com"+message.sender.thumbnailImage,
      
    });

    if(chatterIndex===-1){
      message.counter=1;
      this.chatters.push(message)
    }else{
      this.chatters[chatterIndex].counter++;
      this.chatters[chatterIndex].content=message.content;
    }
    if(this.actifSouhabi.length>0){
    let waitingIndex =this.actifSouhabi.findIndex(stored=>stored.id===message.sender.id);
    if(waitingIndex===-1){
      let newSouhayb:any;
      newSouhayb=message.sender;
      newSouhayb.message=message;
      this.actifSouhabi.push(newSouhayb)
      console.log(this.actifSouhabi);
    }else{
      this.actifSouhabi[waitingIndex].message=message;
    }
  }else{
    let newSouhayb:any;
    newSouhayb=message.sender;
    newSouhayb.message=message;
    this.actifSouhabi.push(newSouhayb);

  }


  }

  goMessage(){
    this.chatters=[];
    let opts:{animation:'ios-transition'}
    this.navCtrl.push('MessagesPage',{waitingChatter : this.actifSouhabi,currentProfile:this.profile},opts)
  }

  goMatches(){
    let matches=[];
    
    this.api.get('matchedsByProfile/'+this.profile.id).subscribe((data)=>{
      data.json().map(notif=>{
       notif.me=this.profile;
       (notif.owner.id===this.profile.id)?(notif.other=notif.wanter,notif.mine=notif.owned,notif.their=notif.wanted):(notif.other=notif.owner,notif.mine=notif.wanted,notif.their=notif.owned);
       notif.distance=notif.distance.toFixed(1);
       delete notif.wanter;
       delete notif.wanted;
       delete notif.owned;
       delete notif.owner;
       matches.push(notif);
       
      })
      this.navCtrl.push('MatchingListPage',{matching:matches});

    })


   
  }


  getListOfChatters() {
    this.getFollowing().subscribe((data)=>{
      //add event
      this.events.publish('following',data.json())
      
      this.following=data.json();
      this.getFollowers().subscribe((data2)=>{
        this.followers=data2.json();
        this.constructSouhabi();
        this.getLastMessages().subscribe(messageArray=>{
          
          this.actifSouhabi=messageArray;
          this.actifSouhabi.sort(function(a,b) {return (a.message.id > b.message.id) ? 1 : ((b.message.id > a.message.id) ? -1 : 0);} ); 
          this.actifSouhabi = this.actifSouhabi.filter(function(n){ return n != undefined }); 
          this.listLoading=false;
          this.actifSouhabi.map((souhayb)=>{
            if(souhayb.message.seenDate===null){
              this.chatters.push(souhayb.message);
          }});
          this.connectAndReceive(this.profile.id);          
        },err=>{
          this.listLoading=false;
          this.connectAndReceive(this.profile.id);
          
          
        },()=>{
          
        });
      })})
 }


 getFollowing(){
   return this.api.get('follows/'+ this.profile.id)
 }
 getFollowers(){
   return this.api.get('followeds/'+ this.profile.id)
 }

 constructSouhabi(){
   let followersProfile=[];
   let followingProfile=[];
   for(let i=0; i<this.followers.length; i++){
     followersProfile[i]=this.followers[i].follower;
   }
   for(let i=0; i<this.following.length; i++){
     followingProfile[i]=this.following[i].followed;
   }


   this.souhabi = _.uniqBy([...followersProfile, ...followingProfile], 'id');
 }

 getLastMessages(): Observable<any> {
  
   if(this.souhabi.length>0){
   let messagesObservable= this.souhabi.map((element,processIdx) => {
     let souhaybId = element.id;
     let uid='';
     
     if(this.profile.id>souhaybId){
       uid=this.profile.id+'naru'+souhaybId;
     }else{
       uid=souhaybId+'naru'+this.profile.id;
     }
     return this.api.get('private-messages/'+ uid,{
       sort:['id,desc'],
       page: 0,
       size: 1,
   }).map(data=>{
     if(data.json().length>0){
     element.message=data.json()[0];
     return element
     }

   })
   .catch((error: any) => {
     return Observable.of(null);
 });     
   });
   return Observable.forkJoin(messagesObservable);
  }else{
    return Observable.throw('terma');
  }
 }

}
