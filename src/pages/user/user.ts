import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,Refresher } from 'ionic-angular';
import {Principal} from "../../providers/providers"
import {Storage} from "@ionic/storage";
import {Api} from "../../providers/api/api"
import { ActionSheetController } from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import { Events } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  profile : any;
  numberOwneds:Number;
  numberWanteds:Number;
  OwnedsItems:any;
  WantedsItems:any;
  ItemsToShow : any;
  choice:string="own";
  isOwn:any;
  me:any;
  myProfile:any;
  followings:any;
  isFriend:any=false;
  isLoading:any=true;
  followIndex:any;
  theFollowing:any;
  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,private principal : Principal,private storage : Storage,private api: Api,public modalCtrl: ModalController,public actionSheetCtrl: ActionSheetController,              private readonly authProvider: AuthProvider,public events: Events
  ) {
    this.profile=this.navParams.get('otherUser');
    this.myProfile=this.navParams.get('currentUser');
   
  
    this.getOwneds(this.profile.id);
    this.getWanteds(this.profile.id); 
    this.getFollowing().subscribe(follow=> {
      console.log('tt')
      this.followings=follow.json();
      console.log(follow)
      this.followIndex=this.followings.findIndex(friend => friend.followed.id === this.profile.id
      )
      if(this.followIndex>-1){
        this.isFriend=true;
        this.theFollowing=this.followings[this.followIndex];
      }
      this.isLoading=false;

    });
    
  }

  getFollowing(){
    return this.api.get('follows/'+ this.myProfile.id)
  }

  showRemoveAlert() {
    let alert = this.alertCtrl.create({
      title: 'It\'s over ?',
      message: 'Stop Following '+this.profile.firstName+' '+this.profile.lastName+' ?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Stop Following',
          handler: () => {
            this.isLoading=true;
            this.api.delete('follows/'+this.theFollowing.id).subscribe(()=>{
              this.followings = this.followings.filter(item => item.id !== this.theFollowing.id);
              this.events.publish('DeletingFollowed',this.theFollowing.followed);
              this.isLoading=false;
              
              this.isFriend=false;
          },err=>{this.isLoading=false;
          });
        }
      }
      ]
    });
    alert.present();
  }

  goToMessage(){
    let uidt =(this.profile.id>this.myProfile.id)? this.profile.id+"naru"+this.myProfile.id : this.myProfile.id+"naru"+this.profile.id;
    this.navCtrl.push('MessageDetailPage',{actualChatter : this.profile,currentProfile:this.myProfile,uid:uidt});
    
  }

  addFriend(){
    this.isLoading=true;
    
    let follow={follower:this.myProfile,followed:this.profile}
    this.api.post('follows',follow).subscribe(data=>{
      this.isFriend=true;
      this.theFollowing=data.json()
      this.isLoading=false;
      
      this.events.publish('addingFollowed',data.json().followed);
    },err=>{
      this.isLoading=false;
    })
    

  }
 

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [
        {
          text: 'Logout',
          handler: () => {
              }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  getOwneds(id){

    this.api.get('owneds/'+id).subscribe((data)=>{
      this.numberOwneds=data.json().length;
      this.OwnedsItems=data.json();
      this.ItemsToShow=data.json();

    })
    
  }

  openModal(article){
    let bookModal = this.navCtrl.push('BookModalPage', { book: article,profile:this.myProfile });
    

  }

  getWanteds(id){
    
        this.api.get('wanteds/'+id).subscribe((data)=>{
          this.numberWanteds=data.json().length;
          this.WantedsItems=data.json();
    
        })
        
  }

  showType(type:Number){
        if (type ===1){
        this.ItemsToShow=this.OwnedsItems;
        } else {
          this.ItemsToShow=this.WantedsItems;
        }
  }


  doPulling(refresher: Refresher) {
  }
 
 
 doRefresh(refresher: Refresher) {

 }

 openModal2(){
  let bookModal = this.modalCtrl.create('ProfileGuidePage');
  bookModal.present();
}

  ionViewDidLoad() {


   }



}
