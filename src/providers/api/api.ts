import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { ResponseWrapper } from '../../models/response-wrapper';
import { createRequestOption } from '../../models/request-util';

import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams,Response } from '@angular/http';
import { AuthHttp} from "angular2-jwt";

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  url: string = 'https://www.netbedloo.com/api';

  constructor(public http: Http,private readonly authHttp: AuthHttp) {
  }
  query(endpoint: string, req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    return this.authHttp.get(this.url + '/' + endpoint, options)
        .map((res: Response) => this.convertResponse(res));
}

  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }
      // Set the search field if we have params and don't already have
      // a search field set in options.
      options.search = !options.search && p || options.search;
    }

    return this.authHttp.get(this.url + '/' + endpoint, options);
  }

  post(endpoint: string, body: any, options?: RequestOptions) {
    return this.authHttp.post(this.url + '/' + endpoint, body, options);
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.authHttp.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.authHttp.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.authHttp.put(this.url + '/' + endpoint, body, options);
  }
  private convertResponse(res: Response): ResponseWrapper {
    const jsonResponse = res.json();
    return new ResponseWrapper(res.headers, jsonResponse, res.status);
}
}
