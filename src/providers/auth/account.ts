import { Injectable } from '@angular/core';
import {Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {SERVER_URL} from "../../config";
import {Storage} from "@ionic/storage";
import {AuthProvider} from "../../providers/auth/auth";
import { AuthHttp} from "angular2-jwt";


@Injectable()

export class AccountService  {
    constructor(private storage: Storage,public auth :AuthProvider,private readonly authHttp: AuthHttp) { }

    get() {

    
        return this.authHttp.get(`${SERVER_URL}/account`).map((res: Response) => res.json());
    }

    save(account: any): Observable<Response> {
        return this.authHttp.post(`${SERVER_URL}/account`, account);
    }
}
