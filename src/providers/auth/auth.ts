import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import {ReplaySubject, Observable} from "rxjs";
import {Storage} from "@ionic/storage";

import {JwtHelper, AuthHttp} from "angular2-jwt";
import {SERVER_URL} from "../../config";

@Injectable()
export class AuthProvider {
  public token : string;
  authUser = new ReplaySubject<any>(1);

  constructor(private readonly http: Http,
              private readonly authHttp: AuthHttp,
              private readonly storage: Storage,
              private readonly jwtHelper: JwtHelper) {

                 var currentUser = JSON.parse(localStorage.getItem('currentUser'));
                        this.token = currentUser && currentUser.token;

  }
 
  getToken() {
    return this.storage.get('authenticationToken');
}
  checkLogin() {
    this.storage.get('authenticationToken').then(jwt => {

      if (jwt && !this.jwtHelper.isTokenExpired(jwt)) {
        this.authHttp.get(`${SERVER_URL}/authenticate`)
          .subscribe(() => this.authUser.next(jwt),
            (err) => this.storage.remove('authenticationToken').then(() => this.authUser.next(null)));
        // OR
        // this.authUser.next(jwt);
      }
      else {
        this.storage.remove('authenticationToken').then(() => this.authUser.next(null));
      }
    });
  }

  login(values: any): Observable<any> {
    
    return this.http.post(`${SERVER_URL}/authenticate`, values).map(res => res.json())
    
    .map(jwt => this.handleJwtResponse(jwt));;
  }



  logout(): Observable<any> {
    return new Observable((observer) => {
      this.storage.remove('authenticationToken').then(() => this.authUser.next(null));
      
        observer.complete();
    });
}

  signup(values: any): Observable<any> {
    return this.http.post(`${SERVER_URL}/register`, values)
  }

  private handleJwtResponse(jwt) {
    jwt=jwt.id_token;

    this.storage.set('isFirstTimeMatching',true);
    this.storage.set('isFirstTimeProfile',true);
    this.storage.set('isFirstTimeMessages',true);
    this.storage.set('isFirstTimeBookModal',true);
    
    
    return this.storage.set('authenticationToken', jwt)
      .then(() => this.authUser.next(jwt))
      .then(() => jwt);
  }

}