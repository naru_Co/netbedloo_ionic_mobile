import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Api } from '../api/api';

/*
  Generated class for the CountriesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CountriesProvider {

  constructor(public http: Http, public api:Api) {
  }

  query(params?: any) {
    return this.api.query('countries', params)
      .map(resp => resp.json);
  }

}
