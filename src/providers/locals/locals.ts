import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Api } from '../api/api';
import {Observable} from 'rxjs/Observable';

import {AuthProvider} from "../../providers/auth/auth";


@Injectable()
export class LocalsProvider {

  constructor(public http: Http,public api:Api,public auth :AuthProvider) {
  }
  get(params?: any) {
    return this.api.get('gouvbyname/'+params)
      .map(resp => {if (resp) {
        return resp.json()
    } else {
        return "";
    }   })
  }

  save(values: any): Observable<any> {

    return this.api.post('gouvs', values)
      .map(response => { return response.json()})
  }

 

}
