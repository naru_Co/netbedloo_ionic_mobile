import { Injectable } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs/Rx';
import { AuthProvider } from '../auth/auth';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';
import 'rxjs/add/operator/map';

/*
  Generated class for the PrivateChatProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PrivateChatProvider {
  stompClient = null;
  subscriber = null;
  connection: Promise<any>;
  connectedPromise: any;
  listener: Observable<any>;
  listenerObserver: Observer<any>;
  alreadyConnectedOnce = false;
  private subscription: Subscription;

  constructor(
    private authProvider : AuthProvider,
  ) {
    this.connection = this.createConnection();
  }

  connect(id1,id2){
    if (this.connectedPromise === null) {
      this.connection = this.createConnection();
    }

    let url = 'https://www.netbedloo.com/websocket/privatechat/';
    this.authProvider.getToken().then(authToken => {;
    if (authToken) {
      url += '?access_token=' + authToken;
  }
  const socket = new SockJS(url);
  this.stompClient = Stomp.over(socket);
  const headers = {};
  this.stompClient.connect(headers, () => {
    this.connectedPromise('success');
    this.connectedPromise = null;
    this.subscribe(id1, id2);
    if (!this.alreadyConnectedOnce) {
 
        this.alreadyConnectedOnce = true;
    }
});
    });
    
  }

  disconnect() {
    if (this.stompClient !== null) {
        this.stompClient.disconnect();
        this.stompClient = null;
    }
    if (this.subscription) {
        this.subscription.unsubscribe();
        this.subscription = null;
    }
    this.alreadyConnectedOnce = false;
}


  private createConnection(): Promise<any> {
    return new Promise((resolve, reject) => this.connectedPromise = resolve);
}

receive() {
  return this.listener;
}

sendMessage(message, id1, id2){
  if (this.stompClient !== null && this.stompClient.connected) {
      this.stompClient.send(
        '/privatechat/' + id1 + '/' + id2, // destination
          JSON.stringify(message), // body
          {} // header
      );
  }
}

subscribe(id1, id2) {
  this.connection.then(() => {
      this.subscriber = this.stompClient.subscribe('/privatechat/private/' + id1 + '/' + id2, (data) => {
          this.listenerObserver.next(JSON.parse(data.body));
      });
  });
}

unsubscribe() {
  if (this.subscriber !== null) {
      this.subscriber.unsubscribe();
  }
  this.listener = this.createListener();
}


private createListener(): Observable<any> {
  return new Observable((observer) => {
      this.listenerObserver = observer;
  });
}



}
